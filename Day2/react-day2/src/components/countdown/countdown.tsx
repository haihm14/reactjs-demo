import React from 'react';
import '../../App.css';

interface CountdownState {
  current: number;
}

export class Countdown extends React.Component<{}, CountdownState> {
  state = {
    current: 10,
  };

  count?: number;

  start = () => {
    this.count = window.setInterval(() => {
      console.log('count: ' + this.count);
      if (this.state.current <= 0) {
        clearInterval(this.count);
        alert('Happy new year 2020');
        return;
      }
      this.setState((state) => {
        if (state.current > 0) {
          return {
            current: state.current - 1,
          };
        }
      });
    }, 1000);
  };

  pause = () => {
    clearInterval(this.count);
  };

  resetDefault = () => {
    this.setState(() => {
        return {
          current: 5,
        };
    });
  };

  componentDidMount() {
    this.start();
  }

  componentWillUnmount() {
    clearInterval(this.count);
  }

  render() {
    const { current } = this.state;
    return (
      <>
        <button className="buttonStart" onClick={this.start}>Start</button>
        <button className="buttonPause" onClick={this.pause}>Pause</button>
        <button className="buttonResetDefault" onClick={this.resetDefault}>Reset Default(5s)</button>
        <div>{current}</div>
      </>
    );
  }
}