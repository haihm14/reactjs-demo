import React from 'react';
import './App.css';

import { Countdown } from './components/countdown/countdown';
import { RatingBar } from './components/rating-bar/rating-bar';

function handleError(e: string) {
  console.error(e);
}

interface UserAvatarProps {
  avatar?: string;
  onError: (v: string) => void;
}

function UserAvatar(props: UserAvatarProps) {
  return React.createElement('img', {
    className: 'img-responsive',
    src: props.avatar,
    alt: 'User Avatar',
    onError: () => props.onError('co loi xay ra'),
  });
}

interface UserProfileProps {
  avatar?: string;
  username: string;
}

function UserProfile(props: UserProfileProps) {
  return (
    <React.Fragment>
      {props.avatar && (
        <UserAvatar avatar={props.avatar} onError={handleError} />
      )}
      <h3 className='username'>{props.username}</h3>
    </React.Fragment>
  );
}
const users = [
  {
    username: 'Hai Hoang',
    avatar:
      'https://i.pinimg.com/originals/5a/27/43/5a2743da81fdd855ddba25c43292f69d.jpg',
  },
  {
    username: 'I\'m Tiger',
    avatar:
      'https://images.unsplash.com/photo-1508817628294-5a453fa0b8fb',
  },
];

interface AppState {
  pgProgress: number;
}

class App extends React.Component<{}, AppState> {
  state = {
    pgProgress: 20,
  };

  increment = () => {
    this.setState((state) => {
      return {
        pgProgress: state.pgProgress + 10,
      };
    });
    console.log(this.state.pgProgress)
  };

  render() {
    return (
      <div className='App'>
        {users.map((user) => (
          <UserProfile key={user.username} {...user} />
        ))}
        <button onClick={this.increment}>Increment</button>

        <hr />
        <Countdown></Countdown>
        <hr />
        <RatingBar max = {10} ratingValue= {3} ></RatingBar>
      </div>
    );
  }
}

export default App;